package name.ekho.experiments.thrift_server;

import org.apache.thrift.TException;

public class TimeServiceHandler implements TimeService.Iface {
    @Override
    public void ping() throws TException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Time get_time() throws TException {
        Time t = new Time();
        t.setText(String.valueOf(System.currentTimeMillis()));
        return t;
    }
}